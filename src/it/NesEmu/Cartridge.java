package it.NesEmu;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import it.NesEmu.mappers.IMapper;
import it.NesEmu.mappers.Mapper000;
import it.NesEmu.mappers.Mapper003;
import it.NesEmu.mappers.Mapper066;
import it.NesEmu.structures.Data;
import it.NesEmu.structures.INesHeader;
import it.NesEmu.structures.MappedAddress;

public class Cartridge {

	private int[] vPRGMemory;
	private int[] vCHRMemory;

	private int nMapperID = 0;
	private int nPRGBanks = 0;
	private int nCHRBanks = 0;

	private IMapper mapper;
	
	public enum MIRROR {
		HORIZONTAL,
		VERTICAL,
		ONESCREEN_LO,
		ONESCREEN_HI
	}
	
	public MIRROR mirror;
	
	private boolean imageValid = false;

	public Cartridge(final String fileName) throws FileNotFoundException {
		
		File game = new File(fileName);
		byte[] rom = new byte[(int) game.length()];
		
		FileInputStream fis = new FileInputStream(game);
		
		try {
			fis.read(rom);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		int[] intRom = new int[rom.length];
		for(int i = 0; i < rom.length; i++)
			intRom[i] = rom[i] & 0xFF;
		
		INesHeader header = new INesHeader(intRom);
		
		imageValid = false;
		
		int trainerOffset = (header.mapper1 & 0x04) > 0? 512 : 0;
		
		nMapperID = ((header.mapper2 >> 4) << 4) | (header.mapper1 >> 4);
		mirror = ((header.mapper1 & 0x01) > 0) ? MIRROR.VERTICAL : MIRROR.HORIZONTAL;
		
		int nFileType = 1;
		
		if(nFileType == 0) {
			
		}
		
		if(nFileType == 1) {
			nPRGBanks = header.prgRomChunks;
			vPRGMemory = new int[nPRGBanks * 16384];
			System.arraycopy(intRom, trainerOffset + 16, vPRGMemory, 0, vPRGMemory.length);
			
			nCHRBanks = header.chrRomChunks;
			vCHRMemory = new int[nCHRBanks * 8192];
			System.arraycopy(intRom, trainerOffset + 16 + vPRGMemory.length, vCHRMemory, 0, vCHRMemory.length);
		}
		
		if(nFileType == 2) {
			
		}
		
		switch(nMapperID) {
		case 0:	mapper = new Mapper000(nPRGBanks, nCHRBanks); break;
		//case   2: mapper = new Mapper002(nPRGBanks, nCHRBanks); break;
		case 3: mapper = new Mapper003(nPRGBanks, nCHRBanks); break;
		case 66: mapper = new Mapper066(nPRGBanks, nCHRBanks); break;
		default:
		}
		
		imageValid = true;
	}
	
	public boolean isImageValid() {
		return imageValid;
	}

	public boolean cpuRead(int addr, Data data) {
		MappedAddress mappedAddr = new MappedAddress();
		
		if(mapper.cpuMapRead(addr, mappedAddr)) {
			data.setData(vPRGMemory[mappedAddr.getMappedAddr()]);
			return true;
		}
		return false;
	}

	public boolean cpuWrite(int addr, int data) {
		MappedAddress mappedAddr = new MappedAddress();
		mapper.data = data;

		if(mapper.cpuMapWrite(addr, mappedAddr)) {
			vPRGMemory[mappedAddr.getMappedAddr()] = data;
			return true;
		}
		return false;
	}

	public boolean ppuRead(int addr, Data data) {
		MappedAddress mappedAddr = new MappedAddress();
		
		if(mapper.ppuMapRead(addr, mappedAddr)) {
			data.setData(vCHRMemory[mappedAddr.getMappedAddr()]);
			return true;
		}
		return false;
	}

	public boolean ppuWrite(int addr, int data) {
		MappedAddress mappedAddr = new MappedAddress();
		if(mapper.ppuMapWrite(addr, mappedAddr)) {
			vCHRMemory[mappedAddr.getMappedAddr()] = data;
			return true;
		}
		return false;
	}
	
	public void reset() {
		if(mapper != null)
			mapper.reset();
	}
}
