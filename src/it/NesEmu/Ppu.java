package it.NesEmu;

import java.awt.Color;

import it.NesEmu.structures.Control;
import it.NesEmu.structures.Data;
import it.NesEmu.structures.LoopyRegister;
import it.NesEmu.structures.Mask;
import it.NesEmu.structures.ObjectAttributeEntry;
import it.NesEmu.structures.Status;
import it.NesEmu.video.PatternTable;
import it.NesEmu.video.SprScreen;

public class Ppu {

	private Cartridge cartridge;

	private int[][] tblName = new int[2][1024];
	private int[] tblPalette = new int[32];
	private int[][] tblPattern = new int[2][4096];

	private Color[] palScreen = new Color[0x40];
	private SprScreen sprScreen = new SprScreen(256, 240);
	private PatternTable[] sprPatternTable = new PatternTable[] { new PatternTable(128, 128, 0),
			new PatternTable(128, 128, 1) };

	private Control control = new Control();
	private Mask mask = new Mask();
	private Status status = new Status();

	public LoopyRegister vramAddr = new LoopyRegister();
	public LoopyRegister tramAddr = new LoopyRegister();

	private int fineX = 0;

	private int addressLatch = 0;
	private int ppuDataBuffer = 0;

	private int scanline = 0;
	private int cycle = 0;

	// BACKGROUND RENDERING =================================
	private int bgNextTileId = 0x00;
	private int bgNextTileAttrib = 0x00;
	private int bgNextTileLsb = 0x00;
	private int bgNextTileMsb = 0x00;
	private int bgShifterPatternLo = 0x0000;
	private int bgShifterPatternHi = 0x0000;
	private int bgShifterAttribLo = 0x0000;
	private int bgShifterAttribHi = 0x0000;
	
	// FOREGROUND RENDERING =================================
	private ObjectAttributeEntry[] OAM = new ObjectAttributeEntry[64];
	private int oamAddr = 0x00;
	private ObjectAttributeEntry[] spriteScanline = new ObjectAttributeEntry[8];
	private int spriteCount;
	private int[] spriteShifterPatternLo = new int[8];
	private int[] spriteShifterPatternHi = new int[8];
	private boolean spriteZeroHitPossible = false;
	private boolean spriteZeroBeingRendered = false;
	
	public int pOAM = 0;
	
	// ======================================================
	public boolean nmi = false;
	public boolean frameComplete = false;

	public Ppu() {
		palScreen[0x00] = new Color(84, 84, 84);
		palScreen[0x01] = new Color(0, 30, 116);
		palScreen[0x02] = new Color(8, 16, 144);
		palScreen[0x03] = new Color(48, 0, 136);
		palScreen[0x04] = new Color(68, 0, 100);
		palScreen[0x05] = new Color(92, 0, 48);
		palScreen[0x06] = new Color(84, 4, 0);
		palScreen[0x07] = new Color(60, 24, 0);
		palScreen[0x08] = new Color(32, 42, 0);
		palScreen[0x09] = new Color(8, 58, 0);
		palScreen[0x0A] = new Color(0, 64, 0);
		palScreen[0x0B] = new Color(0, 60, 0);
		palScreen[0x0C] = new Color(0, 50, 60);
		palScreen[0x0D] = new Color(0, 0, 0);
		palScreen[0x0E] = new Color(0, 0, 0);
		palScreen[0x0F] = new Color(0, 0, 0);

		palScreen[0x10] = new Color(152, 150, 152);
		palScreen[0x11] = new Color(8, 76, 196);
		palScreen[0x12] = new Color(48, 50, 236);
		palScreen[0x13] = new Color(92, 30, 228);
		palScreen[0x14] = new Color(136, 20, 176);
		palScreen[0x15] = new Color(160, 20, 100);
		palScreen[0x16] = new Color(152, 34, 32);
		palScreen[0x17] = new Color(120, 60, 0);
		palScreen[0x18] = new Color(84, 90, 0);
		palScreen[0x19] = new Color(40, 114, 0);
		palScreen[0x1A] = new Color(8, 124, 0);
		palScreen[0x1B] = new Color(0, 118, 40);
		palScreen[0x1C] = new Color(0, 102, 120);
		palScreen[0x1D] = new Color(0, 0, 0);
		palScreen[0x1E] = new Color(0, 0, 0);
		palScreen[0x1F] = new Color(0, 0, 0);

		palScreen[0x20] = new Color(236, 238, 236);
		palScreen[0x21] = new Color(76, 154, 236);
		palScreen[0x22] = new Color(120, 124, 236);
		palScreen[0x23] = new Color(176, 98, 236);
		palScreen[0x24] = new Color(228, 84, 236);
		palScreen[0x25] = new Color(236, 88, 180);
		palScreen[0x26] = new Color(236, 106, 100);
		palScreen[0x27] = new Color(212, 136, 32);
		palScreen[0x28] = new Color(160, 170, 0);
		palScreen[0x29] = new Color(116, 196, 0);
		palScreen[0x2A] = new Color(76, 208, 32);
		palScreen[0x2B] = new Color(56, 204, 108);
		palScreen[0x2C] = new Color(56, 180, 204);
		palScreen[0x2D] = new Color(60, 60, 60);
		palScreen[0x2E] = new Color(0, 0, 0);
		palScreen[0x2F] = new Color(0, 0, 0);

		palScreen[0x30] = new Color(236, 238, 236);
		palScreen[0x31] = new Color(168, 204, 236);
		palScreen[0x32] = new Color(188, 188, 236);
		palScreen[0x33] = new Color(212, 178, 236);
		palScreen[0x34] = new Color(236, 174, 236);
		palScreen[0x35] = new Color(236, 174, 212);
		palScreen[0x36] = new Color(236, 180, 176);
		palScreen[0x37] = new Color(228, 196, 144);
		palScreen[0x38] = new Color(204, 210, 120);
		palScreen[0x39] = new Color(180, 222, 120);
		palScreen[0x3A] = new Color(168, 226, 144);
		palScreen[0x3B] = new Color(152, 226, 180);
		palScreen[0x3C] = new Color(160, 214, 228);
		palScreen[0x3D] = new Color(160, 162, 160);
		palScreen[0x3E] = new Color(0, 0, 0);
		palScreen[0x3F] = new Color(0, 0, 0);
		
		for(int i = 0; i < 8; i++) {
			spriteScanline[i] = new ObjectAttributeEntry();
		}
		
		for(int i = 0; i < 64; i++) {
			OAM[i] = new ObjectAttributeEntry();
		}
	}

	public SprScreen getScreen() {
		return sprScreen;
	}

	public PatternTable getPatternTable(int i, int palette) {
		for (int tileY = 0; tileY < 16; tileY++) {
			for (int tileX = 0; tileX < 16; tileX++) {
				int offset = tileY * 256 + tileX * 16;

				for (int row = 0; row < 8; row++) {
					int tileLsb = ppuRead(i * 0x1000 + offset + row + 0x0000, false); // & 0xFF;
					int tileMsb = ppuRead(i * 0x1000 + offset + row + 0x0008, false); // & 0xFF;

					for (int col = 0; col < 8; col++) {
						int pixel = (tileLsb & 0x01) + ((tileMsb & 0x01) << 1);
						tileLsb >>= 1;
						tileMsb >>= 1;
						sprPatternTable[i].setPixel(tileX * 8 + (7 - col), tileY * 8 + row,
								getColorFromPaletteRam(palette, pixel));
					}
				}
			}
		}

		return sprPatternTable[i];
	}

	public Color getColorFromPaletteRam(int palette, int pixel) {
		return palScreen[ppuRead(0x3F00 + (palette << 2) + pixel, false) & 0x3F];
	}

	public int cpuRead(int addr, boolean readOnly) {
		int data = 0x00;

		if (readOnly) {

			switch (addr) {

			case 0x0000: // Control
				data = control.getReg();
				break;
			case 0x0001: // Mask
				data = mask.getReg();
				break;
			case 0x0002: // Status
				data = status.getReg();
				break;
			case 0x0003: // OAM Address
				break;
			case 0x0004: // OAM Data
				break;
			case 0x0005: // Scroll
				break;
			case 0x0006: // PPU Address
				break;
			case 0x0007: // PPU Data
				break;
			}
		} else {

			switch (addr) {

			case 0x0000:
				break;
			case 0x0001:
				break;
			case 0x0002:
				data = (status.getReg() & 0xE0) | (ppuDataBuffer & 0x1F);
				status.verticalBlank = 0;
				addressLatch = 0;
				break;
			case 0x0003:
				break;

			// OAM Data
			case 0x0004:
				data = getOamData(oamAddr);
				break;

			// Scroll - Not Readable
			case 0x0005:
				break;

			// PPU Address - Not Readable
			case 0x0006:
				break;

			// PPU Data
			case 0x0007:
				data = ppuDataBuffer;
				ppuDataBuffer = ppuRead(vramAddr.getReg(), false);
				if (vramAddr.getReg() >= 0x3F00)
					data = ppuDataBuffer;
				if (control.incrementMode == 1) {
					vramAddr.coarseY = (vramAddr.coarseY + 1); // & 0x1F;
				} else {
					vramAddr.coarseX = (vramAddr.coarseX + 1); // & 0x1F;
				}
				break;
			}
		}

		return data;
	}

	public void cpuWrite(int addr, int data) {
		switch (addr) {
		case 0x0000: // Control
			control.setReg(data);
			tramAddr.nametableX = control.nametableX;
			tramAddr.nametableY = control.nametableY;
			break;
		case 0x0001: // Mask
			mask.setReg(data);
			break;
		case 0x0002: // Status
			break;
		case 0x0003: // OAM Address
			oamAddr = data;
			break;
		case 0x0004: // OAM Data
			setOamData(oamAddr, data);
			break;
		case 0x0005: // Scroll
			if (addressLatch == 0) {
				fineX = data & 0x07;
				tramAddr.coarseX = data >> 3;
				addressLatch = 1;
			} else {
				tramAddr.fineY = data & 0x07;
				tramAddr.coarseY = data >> 3;
				addressLatch = 0;
			}
			break;
		case 0x0006: // PPU Address
			if (addressLatch == 0) {
				tramAddr.setReg((((data & 0x3F) << 8) | (tramAddr.getReg() & 0x00FF)) /*& 0xFFFF*/);
				addressLatch = 1;
			} else {
				tramAddr.setReg(((tramAddr.getReg() & 0xFF00) | data) /*& 0xFFFF*/);
				vramAddr.setReg(tramAddr.getReg());
				addressLatch = 0;
			}
			break;
		case 0x0007: // PPU Data
			ppuWrite(vramAddr.getReg(), data);
			if (control.incrementMode > 0) {
				vramAddr.coarseY = (vramAddr.coarseY + 1); // & 0x1F;
			} else {
				vramAddr.coarseX = (vramAddr.coarseX + 1); // & 0x1F;
			}
			break;
		default:
		}
	}

	public int ppuRead(int addr, boolean readOnly) {
		Data data = new Data();
		addr &= 0x3FFF;

		if (cartridge != null && cartridge.ppuRead(addr, data)) {

		} else if (addr >= 0x0000 && addr <= 0x1FFF) {
			data.setData(tblPattern[(addr & 0x1000) >> 12][addr & 0x0FFF]);
		} else if (addr >= 0x2000 && addr <= 0x3EFF) {
			addr &= 0x0FFF;

			if (cartridge.mirror == Cartridge.MIRROR.VERTICAL) {

				if (addr >= 0x0000 && addr <= 0x03FF)
					data.setData(tblName[0][addr & 0x03FF]);
				if (addr >= 0x0400 && addr <= 0x07FF)
					data.setData(tblName[1][addr & 0x03FF]);
				if (addr >= 0x0800 && addr <= 0x0BFF)
					data.setData(tblName[0][addr & 0x03FF]);
				if (addr >= 0x0C00 && addr <= 0x0FFF)
					data.setData(tblName[1][addr & 0x03FF]);

			} else if (cartridge.mirror == Cartridge.MIRROR.HORIZONTAL) {

				if (addr >= 0x0000 && addr <= 0x03FF)
					data.setData(tblName[0][addr & 0x03FF]);
				if (addr >= 0x0400 && addr <= 0x07FF)
					data.setData(tblName[0][addr & 0x03FF]);
				if (addr >= 0x0800 && addr <= 0x0BFF)
					data.setData(tblName[1][addr & 0x03FF]);
				if (addr >= 0x0C00 && addr <= 0x0FFF)
					data.setData(tblName[1][addr & 0x03FF]);

			}
		} else if (addr >= 0x3F00 && addr <= 0x3FFF) {
			addr &= 0x001F;
			if (addr == 0x0010)
				addr = 0x0000;
			if (addr == 0x0014)
				addr = 0x0004;
			if (addr == 0x0018)
				addr = 0x0008;
			if (addr == 0x001C)
				addr = 0x000C;
			
			data.setData(tblPalette[addr] & (mask.grayscale > 0 ? 0x30 : 0x3F));
		}

		return data.getData();
	}

	public void ppuWrite(int addr, int data) {
		addr &= 0x3FFF;

		if (cartridge.ppuWrite(addr, data)) {

		} else if (addr >= 0x0000 && addr <= 0x1FFF) {
			tblPattern[(addr & 0x1000) >> 12][addr & 0x0FFF] = data;
		} else if (addr >= 0x2000 && addr <= 0x3EFF) {
			addr &= 0x0FFF;

			if (cartridge.mirror == Cartridge.MIRROR.VERTICAL) {
				if (addr >= 0x0000 && addr <= 0x03FF)
					tblName[0][addr & 0x03FF] = data;
				if (addr >= 0x0400 && addr <= 0x07FF)
					tblName[1][addr & 0x03FF] = data;
				if (addr >= 0x0800 && addr <= 0x0BFF)
					tblName[0][addr & 0x03FF] = data;
				if (addr >= 0x0C00 && addr <= 0x0FFF)
					tblName[1][addr & 0x03FF] = data;
			} else if (cartridge.mirror == Cartridge.MIRROR.HORIZONTAL) {
				if (addr >= 0x0000 && addr <= 0x03FF)
					tblName[0][addr & 0x03FF] = data;
				if (addr >= 0x0400 && addr <= 0x07FF)
					tblName[0][addr & 0x03FF] = data;
				if (addr >= 0x0800 && addr <= 0x0BFF)
					tblName[1][addr & 0x03FF] = data;
				if (addr >= 0x0C00 && addr <= 0x0FFF)
					tblName[1][addr & 0x03FF] = data;
			}
		} else if (addr >= 0x3F00 && addr <= 0x3FFF) {
			addr &= 0x001F;
			if (addr == 0x0010)
				addr = 0x0000;
			if (addr == 0x0014)
				addr = 0x0004;
			if (addr == 0x0018)
				addr = 0x0008;
			if (addr == 0x001C)
				addr = 0x000C;
			tblPalette[addr] = data;
		}
	}

	public void connectCartridge(final Cartridge cartridge) {
		this.cartridge = cartridge;
	}

	public void reset() {
		fineX = 0x00;
		addressLatch = 0x00;
		ppuDataBuffer = 0x00;
		scanline = 0;
		cycle = 0;
		bgNextTileId = 0x00;
		bgNextTileAttrib = 0x00;
		bgNextTileLsb = 0x00;
		bgNextTileMsb = 0x00;
		bgShifterPatternLo = 0x0000;
		bgShifterPatternHi = 0x0000;
		bgShifterAttribLo = 0x0000;
		bgShifterAttribHi = 0x0000;
		status.setReg(0x00);
		mask.setReg(0x00);
		control.setReg(0x00);
		vramAddr.setReg(0x0000);
		tramAddr.setReg(0x0000);
	}

	public void clock() {
		if (scanline >= -1 && scanline < 240) {

			if (scanline == 0 && cycle == 0)
				cycle = 1;

			if (scanline == -1 && cycle == 1) {
				status.verticalBlank = 0;
				status.spriteOverflow = 0;
				status.spriteZeroHit = 0;
				
				for (int i = 0; i < 8; i++) {
					spriteShifterPatternLo[i] = 0;
					spriteShifterPatternHi[i] = 0;
				}
			}

			if ((cycle >= 2 && cycle < 258) || (cycle >= 321 && cycle < 338)) {
				updateShifters();

				switch ((cycle - 1) % 8) {
				case 0:
					loadBackgroundShifters();
					bgNextTileId = ppuRead(0x2000 | (vramAddr.getReg() & 0x0FFF), false);
					break;

				case 2:
					bgNextTileAttrib = ppuRead(0x23C0 | (vramAddr.nametableY << 11) | (vramAddr.nametableX << 10)
							| ((vramAddr.coarseY >> 2) << 3) | (vramAddr.coarseX >> 2), false);
					if ((vramAddr.coarseY & 0x02) > 0)
						bgNextTileAttrib >>= 4;
					if ((vramAddr.coarseX & 0x02) > 0)
						bgNextTileAttrib >>= 2;
					bgNextTileAttrib &= 0x03;
					break;

				case 4:
					bgNextTileLsb = ppuRead(
							(control.patternBackground << 12) + ((bgNextTileId << 4) /*& 0xFFFF*/) + vramAddr.fineY + 0,
							false);
					break;

				case 6:
					bgNextTileMsb = ppuRead(
							(control.patternBackground << 12) + ((bgNextTileId << 4) /*& 0xFFFF*/) + vramAddr.fineY + 8, false);
					break;

				case 7:
					incrementScrollX();
					break;

				default:
				}
			}

			if (cycle == 256)
				incrementScrollY();

			if (cycle == 257) {
				loadBackgroundShifters();
				transferAddressX();
			}

			if (cycle == 338 || cycle == 340)
				bgNextTileId = ppuRead(0x2000 | (vramAddr.getReg() & 0x0FFF), false);

			if (scanline == -1 && cycle >= 280 && cycle < 305)
				transferAddressY();
			
			// FOREGROUND RENDERING ================================================
			if(cycle == 257 && scanline >= 0) {
				clearSpriteScanline();
				
				spriteCount = 0;
				
				for(int i = 0; i < 8; i++) {
					spriteShifterPatternLo[i] = 0;
					spriteShifterPatternHi[i] = 0;
				}
				
				int oamEntry = 0;
				spriteZeroHitPossible = false;
				
				while(oamEntry < 64 && spriteCount < 9) {
					short diff = (short) ((short)scanline - (short)OAM[oamEntry].y);// & 0xFFFF;
					
					if(diff >= 0 && diff < (control.spriteSize > 0 ? 16 : 8)) {
						if(spriteCount < 8) {
							if(oamEntry == 0) {
								spriteZeroHitPossible = true;
							}
							
							spriteScanline[spriteCount].x = OAM[oamEntry].x;
							spriteScanline[spriteCount].y = OAM[oamEntry].y;
							spriteScanline[spriteCount].attribute = OAM[oamEntry].attribute;
							spriteScanline[spriteCount].id = OAM[oamEntry].id;
							spriteCount++;
						}
					}
					
					oamEntry++;
				}
				
				status.spriteOverflow = (spriteCount >> 8);
			}
			
			if(cycle == 340) {
				for(int i = 0; i < spriteCount; i++) {
					int spritePatternBitsLo, spritePatternBitsHi;
					int spritePatternAddrLo, spritePatternAddrHi;
					
					if(control.spriteSize == 0) {
						if((spriteScanline[i].attribute & 0x80) == 0) {
							spritePatternAddrLo = (
									(control.patternSprite << 12)
									| (spriteScanline[i].id << 4)
									| (scanline - spriteScanline[i].y) & 0xFF) & 0xFFFF;
						} else {
							spritePatternAddrLo = (
									(control.patternSprite << 12)
									| (spriteScanline[i].id << 4)
									| (7 - (scanline - spriteScanline[i].y)) & 0xFF) & 0xFFFF;
						}
					} else {
						if((spriteScanline[i].attribute & 0x80) == 0) {
							if(((scanline - spriteScanline[i].y) & 0xFF) < 8) {
								spritePatternAddrLo = 
										(((spriteScanline[i].id & 0x01)      << 12)  // Which Pattern Table? 0KB or 4KB offset
										| ((spriteScanline[i].id & 0xFE)      << 4 )  // Which Cell? Tile ID * 16 (16 bytes per tile)
										| ((scanline - spriteScanline[i].y) & 0x07 )) & 0xFFFF;
							} else {
								spritePatternAddrLo =
										(((spriteScanline[i].id & 0x01)      << 12)  // Which Pattern Table? 0KB or 4KB offset
										| (((spriteScanline[i].id & 0xFE) + 1) << 4 )  // Which Cell? Tile ID * 16 (16 bytes per tile)
										| ((scanline - spriteScanline[i].y) & 0x07 ) & 0xFFFF);
							}
						} else {
							if(((scanline - spriteScanline[i].y) & 0xFF) < 8) {
								spritePatternAddrLo = 
										(((spriteScanline[i].id & 0x01)      << 12)    // Which Pattern Table? 0KB or 4KB offset
										| (((spriteScanline[i].id & 0xFE) + 1) << 4 )    // Which Cell? Tile ID * 16 (16 bytes per tile)
										| (7 - (scanline - spriteScanline[i].y) & 0x07)) & 0xFFFF;
							} else {
								spritePatternAddrLo =
										(((spriteScanline[i].id & 0x01)       << 12)    // Which Pattern Table? 0KB or 4KB offset
										| ((spriteScanline[i].id & 0xFE)       << 4 )    // Which Cell? Tile ID * 16 (16 bytes per tile)
										| (7 - (scanline - spriteScanline[i].y) & 0x07)) & 0xFFFF;
							}
						}
					}
					
					spritePatternAddrHi = spritePatternAddrLo + 8;
					
					spritePatternBitsLo = ppuRead(spritePatternAddrLo, false);
					spritePatternBitsHi = ppuRead(spritePatternAddrHi, false);
					
					if((spriteScanline[i].attribute & 0x40) > 0) {
						spritePatternBitsLo = flipByte(spritePatternBitsLo);
						spritePatternBitsHi = flipByte(spritePatternBitsHi);
					}
					
					spriteShifterPatternLo[i] = spritePatternBitsLo;
					spriteShifterPatternHi[i] = spritePatternBitsHi;
				}
			}
		}

		if (scanline == 240) {
			// Do nothing
		}

		if (scanline >= 241 && scanline < 261) {
			if (scanline == 241 && cycle == 1) {
				status.verticalBlank = 1;

				if (control.enableNmi > 0)
					nmi = true;
			}
		}

		int bgPixel = 0x00;
		int bgPalette = 0x00;

		if(mask.renderBackground > 0) {
			int bitMux = 0x8000 >> fineX;
							
			int p0Pixel = (bgShifterPatternLo & bitMux) > 0 ? 1 : 0;
			int p1Pixel = (bgShifterPatternHi & bitMux) > 0 ? 1 : 0;

			bgPixel = (p1Pixel << 1) | p0Pixel;

			int bgPal0 = (bgShifterAttribLo & bitMux) > 0 ? 1 : 0;
			int bgPal1 = (bgShifterAttribHi & bitMux) > 0 ? 1 : 0;

			bgPalette = (bgPal1 << 1) | bgPal0;
		}
		
		// FOREGROUND =====================================
		int fgPixel = 0x00;
		int fgPalette = 0x00;
		int fgPriority = 0x00;
		if(mask.renderSprites > 0) {
			spriteZeroBeingRendered = false;
			
			for(int i = 0; i < spriteCount; i++) {
				if(spriteScanline[i].x == 0) {
					int fgPixelLo = (spriteShifterPatternLo[i] & 0x80) > 0? 1 : 0;
					int fgPixelHi = (spriteShifterPatternHi[i] & 0x80) > 0? 1 : 0;
					fgPixel = (fgPixelHi << 1) | fgPixelLo;
					
					fgPalette = (spriteScanline[i].attribute & 0x03) + 0x04;
					fgPriority = (spriteScanline[i].attribute & 0x20) == 0? 1 : 0;
					
					if(fgPixel != 0) {
						if(i == 0)
							spriteZeroBeingRendered = true;
						break;
					}
				}
			}
		}

		int pixel = 0x00;
		int palette = 0x00;
		
		if(bgPixel == 0 && fgPixel == 0) {
			pixel = 0x00;
			palette = 0x00;
			
		} else if(bgPixel == 0 && fgPixel > 0) {
			pixel = fgPixel;
			palette = fgPalette;
			
		} else if(bgPixel > 0 && fgPixel == 0) {
			pixel = bgPixel;
			palette = bgPalette;
			
		} else if(bgPixel > 0 && fgPixel > 0) {
			if(fgPriority > 0) {
				pixel = fgPixel;
				palette = fgPalette;
			} else {
				pixel = bgPixel;
				palette = bgPalette;
			}
			
			if(spriteZeroHitPossible && spriteZeroBeingRendered) {
				if((mask.renderBackground & mask.renderSprites) > 0) {
					if((~(mask.renderBackgroundLeft | mask.renderSpritesLeft)) != 0) {
						if(cycle >= 9 && cycle < 258)
							status.spriteZeroHit = 1;
					} else {
						if(cycle >= 1 && cycle < 258)
							status.spriteZeroHit = 1;
					}
				}
			}
		}
		
		sprScreen.setPixel(cycle - 1, scanline, getColorFromPaletteRam(palette, pixel));
		// sprScreen.setPixel(cycle - 1, scanline, palScreen[(Math.random() % 2 > 0) ?
		// 0x3F : 0x30]);
		
//		if(vramAddr.coarseX > 31)
//			System.out.println("vramAddr.coarseX > 31!");
		
		cycle++;
		if (cycle >= 341) {
			cycle = 0;
			scanline++;

			if (scanline >= 261) {
				scanline = -1;
				frameComplete = true;
			}
		}
	}

	private void incrementScrollX() {
		if ((mask.renderBackground > 0) || (mask.renderSprites > 0)) {
			if (vramAddr.coarseX == 31) {
				vramAddr.coarseX = 0;
				vramAddr.nametableX = (~vramAddr.nametableX) & 0x01;

			} else {
				vramAddr.coarseX = (vramAddr.coarseX + 1);// & 0x1F;
			}
		}
	}

	private void incrementScrollY() {
		if ((mask.renderBackground > 0) || (mask.renderSprites > 0)) {
			if (vramAddr.fineY < 7) {
				vramAddr.fineY = (vramAddr.fineY + 1);// & 0x7;

			} else {
				vramAddr.fineY = 0;

				if (vramAddr.coarseY == 29) {
					vramAddr.coarseY = 0;
					vramAddr.nametableY = (~vramAddr.nametableY) & 0x01;

				} else if (vramAddr.coarseY == 31) {
					vramAddr.coarseY = 0;

				} else {
					vramAddr.coarseY = (vramAddr.coarseY + 1); // & 0x1F;
				}
			}
		}
	}

	private void transferAddressX() {
		if ((mask.renderBackground > 0) || (mask.renderSprites > 0)) {
			vramAddr.nametableX = tramAddr.nametableX;
			vramAddr.coarseX = tramAddr.coarseX;
		}
	}

	private void transferAddressY() {
		if ((mask.renderBackground > 0) || (mask.renderSprites > 0)) {
			vramAddr.fineY = tramAddr.fineY;
			vramAddr.nametableY = tramAddr.nametableY;
			vramAddr.coarseY = tramAddr.coarseY;
		}
	}

	private void loadBackgroundShifters() {
		bgShifterPatternLo = (bgShifterPatternLo & 0xFF00) | bgNextTileLsb;
		bgShifterPatternHi = (bgShifterPatternHi & 0xFF00) | bgNextTileMsb;

		bgShifterAttribLo = (bgShifterAttribLo & 0xFF00) | ((bgNextTileAttrib & 0b01) > 0 ? 0xFF : 0x00);
		bgShifterAttribHi = (bgShifterAttribHi & 0xFF00) | ((bgNextTileAttrib & 0b10) > 0 ? 0xFF : 0x00);
	}

	private void updateShifters() {
		if (mask.renderBackground > 0) {
			bgShifterPatternLo = (bgShifterPatternLo << 1);// & 0xFFFF;
			bgShifterPatternHi = (bgShifterPatternHi << 1);// & 0xFFFF;

			bgShifterAttribLo = (bgShifterAttribLo << 1);// & 0xFFFF;
			bgShifterAttribHi = (bgShifterAttribHi << 1);// & 0xFFFF;
		}
		
		if(mask.renderSprites > 0 && cycle >= 1 && cycle < 258) {
			for(int i = 0; i < spriteCount; i++) {
				
				if(spriteScanline[i].x > 0)
					spriteScanline[i].x = (spriteScanline[i].x - 1);
				else {
					spriteShifterPatternLo[i] = (spriteShifterPatternLo[i] << 1);// & 0xFF;
					spriteShifterPatternHi[i] = (spriteShifterPatternHi[i] << 1);// & 0xFF;
				}
			}
		}
	}
	
	private void clearSpriteScanline() {
		for(ObjectAttributeEntry entry : spriteScanline) {
			entry.attribute = 0xFF;
			entry.id = 0xFF;
			entry.x = 0xFF;
			entry.y = 0xFF;
		}
	}
	
	private int flipByte(int b) {
		b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
		b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
		b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
		return b;
	}
	
	public void setOamData(int addr, int data) {
		int index = addr / 4;
		
		switch (addr % 4) {
		case 0:
			OAM[index].y = data;
			break;
		case 1:
			OAM[index].id = data;
			break;
		case 2:
			OAM[index].attribute = data;
			break;
		case 3:
			OAM[index].x = data;
			break;
		default:
		}
	}
	
	public int getOamData(int addr) {
		int index = addr / 4;
		int data = 0;
		
		switch (addr % 4) {
		case 0:
			data = OAM[index].y;
			break;
		case 1:
			data = OAM[index].id;
			break;
		case 2:
			data = OAM[index].attribute;
			break;
		case 3:
			data = OAM[index].x;
			break;
		default:
		}
		return data;
	}
}
