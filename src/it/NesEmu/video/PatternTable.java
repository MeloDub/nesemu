package it.NesEmu.video;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import it.NesEmu.tests.TestPpu;

public class PatternTable extends JPanel implements Runnable {

	private static final long serialVersionUID = 1L;
	
	private int w = 0;
	private int h = 0;
	private int x = 0;
	private int y = 0;
	private int index;
	
	Color[] screen;

	Thread pTableThread;
	
	public PatternTable(int width, int height, int index) {
		w = width;
		h = height;
		this.index = index;
		screen = new Color[width * height];
		setLayout(null);
		
		(pTableThread = new Thread(this)).start();
	}
	
	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void setPixel(int x, int y, Color color) {
		if(x >= 0 && x < w && y >= 0 && y < h)
			screen[y * w + x] = color;
	}
	
	public Color getPixel(int x, int y) {
		if(x >= 0 && x < w && y >= 0 && y < h)
			return screen[y * w + x];
		return new Color(0, 0, 0);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		for(int r = 0; r < h; r++) {
			for(int c = 0; c < w; c++) {
				g.setColor(getPixel(c, r));
				g.fillRect(c + x, r + y, 1, 1);
			}
		}
	}
	
	public Color[] getScreen() {
		return screen;
	}
	
	@Override
	public void run() {
		while (Thread.currentThread() == pTableThread) {
//			try {
//				this.pTableThread.sleep(200);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
			
			TestPpu.nes.ppu.getPatternTable(index, 0);
			repaint();
		}
	}

}
