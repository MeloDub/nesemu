package it.NesEmu.video;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import javax.swing.JPanel;

import it.NesEmu.Cartridge;
import it.NesEmu.tests.TestPpu;

public class SprScreen extends JPanel implements Runnable {

	private static final long serialVersionUID = 1979553474603467869L;
	
	private int w = 0;
	private int h = 0;
	private Color[] screen = null;
	
	private int scale = 3;
	
	private static Map<Integer, String> mapAsm;
	
	Thread gameThread;
	
	public SprScreen(int width, int height) {
		this.h = height;
		this.w = width;
		
		screen = new Color[width * height];
		
		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				printRegisters();
				printCode(516, 72, 26);
				printRam(0xC000, 16, 16);
				printRam(0x2000, 16, 16);			
			}
		});
		
		(gameThread = new Thread(this)).start();
	}
	
	public void setPixel(int x, int y, Color color) {
		if(x >= 0 && x < w && y >= 0 && y < h)
			screen[y * w + x] = color;
	}
	
	public Color getPixel(int x, int y) {
		if(x >= 0 && x < w && y >= 0 && y < h)
			return screen[y * w + x];
		return new Color(0, 0, 0);
	}
	
	@Override
	public void paintComponent(Graphics gg) {
		super.paintComponent(gg);
		
		drawScreen(gg);
		drawPatternTable(gg);
		
	}
	
	private void drawScreen(Graphics g) {
		for(int r = 0; r < h; r++) {
			for(int c = 0; c < w; c++) {
				g.setColor(getPixel(c, r));
				g.fillRect(c*scale, r*scale, scale, scale);
			}
		}
	}
	
	private void drawPatternTable(Graphics g) {
		int initialRow = 464;
		int initialColumn = 828;
		int h = 128;
		int w = 128;
		int scale = 2;
		
		Color[] screen = TestPpu.nes.ppu.getPatternTable(0, 0).getScreen();
		
		for(int r = 0; r < h; r++) {
			for(int c = 0; c < w; c++) {
				g.setColor(screen[r * w + c]);
				g.fillRect((c*scale + initialColumn), (r*scale + initialRow), scale, scale);
			}
		}
		
		initialColumn = 1134;
		screen = TestPpu.nes.ppu.getPatternTable(1, 0).getScreen();
		
		for(int r = 0; r < h; r++) {
			for(int c = 0; c < w; c++) {
				g.setColor(screen[r * w + c]);
				g.fillRect((c*scale + initialColumn), (r*scale + initialRow), scale, scale);
			}
		}
	}

	@Override
	public void run() {
		try {
			TestPpu.nes.insertCartridge(new Cartridge("D:\\Download\\Thunder Lightning.nes"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		TestPpu.nes.reset();
		
		mapAsm = TestPpu.nes.cpu.disassemble(0x0000, 0xFFFF);
		
		while (Thread.currentThread() == gameThread) {
			try {
				Thread.sleep(5);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			do {
				try {
					TestPpu.nes.clock();
				} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
					e.printStackTrace();
				}
			} while (!TestPpu.nes.ppu.frameComplete);
			TestPpu.nes.ppu.frameComplete = false;
			repaint();
		}
	}
	
	public static void printRam(int startAddr, int rows, int columns) {
		for(int i = 0; i < rows ; i++) {
			
			StringBuilder riga = new StringBuilder();
			riga.append("$" + convert(startAddr, true) + ": ");
			
			for(int j = 0; j < columns; j++) {
				riga.append(convert(TestPpu.nes.cpuRead(startAddr, true), false) + " ");
				startAddr++;
			}
			
			System.out.println(riga);
		}
		System.out.println("\n");
	}
	
	private static void printRegisters() {
		System.out.println("A: " + convert(TestPpu.nes.cpu.a, false) + " [" + TestPpu.nes.cpu.a + "]");
		System.out.println("X: " + convert(TestPpu.nes.cpu.x, false) + " [" + TestPpu.nes.cpu.x + "]");
		System.out.println("Y: " + convert(TestPpu.nes.cpu.y, false) + " [" + TestPpu.nes.cpu.y + "]");
		System.out.println("PC: " + convert(TestPpu.nes.cpu.pc, true) + " [" + TestPpu.nes.cpu.pc + "]");
		System.out.println("Stack p: " + convert(TestPpu.nes.cpu.stkp, false) + "\n");
	}
	
	public static void printCode(int x, int y, int lines) {
		String it_a = mapAsm.get(TestPpu.nes.cpu.pc);
//		int lineY = (lines >> 1) * 10 + y;
		
		if(it_a != null) {
			System.out.println(it_a + "\n");
//			while(lineY < (lines * 10) + y) {
//				lineY += 10;
				
			}
		}
	
	public static String convert(int n, boolean isAddr) {
		  String ret = Integer.toHexString(n);
		  if(isAddr) {
			  if(ret.length() == 3)
				  ret = "0" + ret;
			  if(ret.length() == 2)
				  ret = "00" + ret;
			  if(ret.length() == 1)
				  ret = "000" + ret;
		  } else {
			  if(ret.length() == 1)
				  ret = "0" + ret;
		  }
		  
		  return ret.toUpperCase();
		}
}
