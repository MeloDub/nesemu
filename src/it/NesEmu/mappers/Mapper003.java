package it.NesEmu.mappers;

import it.NesEmu.structures.MappedAddress;

public class Mapper003 extends IMapper {

	private int chrBankSelect = 0;

	public Mapper003(int prgBanks, int chrBanks) {
		super(prgBanks, chrBanks);
	}

	@Override
	public boolean cpuMapRead(int addr, MappedAddress mappedAddr) {
		if (addr >= 0x8000 && addr <= 0xFFFF) {
			mappedAddr.setMappedAddr(addr & (nPRGBanks > 1 ? 0x7FFF : 0x3FFF));
			return true;
		}

		return false;
	}

	@Override
	public boolean cpuMapWrite(int addr, MappedAddress mappedAddr) {
		if (addr >= 0x8000 && addr <= 0xFFFF) {
			chrBankSelect = (data % nCHRBanks);
			mappedAddr.setMappedAddr(addr & (nPRGBanks > 1 ? 0x7FFF : 0x3FFF));
			return true;
		}

		return false;
	}

	@Override
	public boolean ppuMapRead(int addr, MappedAddress mappedAddr) {
		if (addr >= 0x0000 && addr <= 0x1FFF) {
			mappedAddr.setMappedAddr(addr + (chrBankSelect * 0x2000));
			return true;
		}

		return false;
	}

	@Override
	public boolean ppuMapWrite(int addr, MappedAddress mappedAddr) {
		if (addr >= 0x0000 && addr <= 0x1FFF) {
			mappedAddr.setMappedAddr(addr + (chrBankSelect * 0x2000));
			return true;
		}
		return false;
	}

	@Override
	public void reset() {
		// Nothing to do
	}

}
