package it.NesEmu.mappers;

import it.NesEmu.structures.MappedAddress;

public class Mapper066 extends IMapper {
	
	private int prgBankSelect = 0;
	private int chrBankSelect = 0;

	public Mapper066(int prgBanks, int chrBanks) {
		super(prgBanks, chrBanks);
	}

	@Override
	public boolean cpuMapRead(int addr, MappedAddress mappedAddr) {
		if (addr >= 0x8000 && addr <= 0xFFFF) {
			mappedAddr.setMappedAddr(addr + (prgBankSelect * 0x8000));
			return true;
		}

		return false;
	}

	@Override
	public boolean cpuMapWrite(int addr, MappedAddress mappedAddr) {
		if (addr >= 0x8000 && addr <= 0xFFFF) {
			chrBankSelect = (data % 4);
			prgBankSelect = ((data & 0x30) >> 4) % (nPRGBanks / 2);
			mappedAddr.setMappedAddr(addr + (prgBankSelect * 0x8000));
			return true;
		}

		return false;
	}

	@Override
	public boolean ppuMapRead(int addr, MappedAddress mappedAddr) {
		if (addr >= 0x0000 && addr <= 0x1FFF) {
			mappedAddr.setMappedAddr(addr + (chrBankSelect * 0x2000));
			return true;
		}

		return false;
	}

	@Override
	public boolean ppuMapWrite(int addr, MappedAddress mappedAddr) {
		if (addr >= 0x0000 && addr <= 0x1FFF) {
			mappedAddr.setMappedAddr(addr + (chrBankSelect * 0x2000));
			return true;
		}
		return false;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}

}
