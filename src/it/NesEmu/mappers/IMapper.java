package it.NesEmu.mappers;

import it.NesEmu.structures.MappedAddress;

public abstract class IMapper {
	
	int nPRGBanks;
	int nCHRBanks;
	public int data;
	
	public IMapper(int prgBanks, int chrBanks) {
		nPRGBanks = prgBanks;
		nCHRBanks = chrBanks;
	}
	
	public abstract boolean cpuMapRead(int addr, MappedAddress mappedAddr);
	public abstract boolean cpuMapWrite(int addr, MappedAddress mappedAddr);
	public abstract boolean ppuMapRead(int addr, MappedAddress mappedAddr);
	public abstract boolean ppuMapWrite(int addr, MappedAddress mappedAddr);
	public abstract void reset();
	
}
