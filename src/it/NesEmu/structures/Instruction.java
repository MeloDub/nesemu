package it.NesEmu.structures;

public class Instruction {

	public String name;
	public String operate;
	public String addrmode;
	public int cycles = 0;
	
	public Instruction(String name, String operate, String addrmode, int cycles) {
		this.name = name;
		this.operate = operate;
		this.addrmode = addrmode;
		this.cycles = cycles;
	}
}
