package it.NesEmu.structures;

public class MappedAddress {
	
	private int mappedAddr = 0;

	public int getMappedAddr() {
		return mappedAddr;
	}

	public void setMappedAddr(int mappedAddr) {
		this.mappedAddr = mappedAddr & 0xFFFF;
	}

}
