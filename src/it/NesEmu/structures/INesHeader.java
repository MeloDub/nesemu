package it.NesEmu.structures;

public class INesHeader {

	public char[] name = new char[4];
	public int prgRomChunks;
	public int chrRomChunks;
	public int mapper1;
	public int mapper2;
	public int prgRamSize;
	public int tvSystem1;
	public int tvSystem2;
	public char[] unused = new char[5];

	public INesHeader(int[] rom) {
		for (int i = 0; i < 4; i++)
			name[i] = (char) rom[i];

		prgRomChunks = rom[4];
		chrRomChunks = rom[5];
		mapper1 = rom[6];
		mapper2 = rom[7];
		prgRamSize = rom[8];
		tvSystem1 = rom[9];
		tvSystem2 = rom[10];

		for (int i = 0; i < 5; i++)
			unused[i] = (char) rom[i + 11];
	}
}
