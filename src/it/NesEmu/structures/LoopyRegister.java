package it.NesEmu.structures;

public class LoopyRegister {

	public int unused;		// MSB   // 1 bit
	public int fineY;                // 3 bit
	public int nametableY;           // 1 bit
	public int nametableX;           // 1 bit
	public int coarseY;              // 5 bit
	public int coarseX;		// LSB   // 5 bit
	
	public int getReg() {
		int reg = 0x00;

		reg += (unused << 15);	  
		reg += (fineY << 12);    
		reg += (nametableY << 11); 
		reg += (nametableX << 10); 
		reg += (coarseY << 5);	  
		reg += (coarseX << 0);    

		return reg;
	}
	
	public void setReg(int data) {
		unused = (data & 0x8000) > 0? 1: 0; 
		fineY = (data & 0x7000) >> 12; 
		nametableY = (data & 0x0800) > 0? 1: 0;       
		nametableX = (data & 0x0400) > 0? 1: 0;
		coarseY = (data & 0x03E0) >> 5;    
		coarseX = (data & 0x001F);    
	}
	
	public void setCoarseX(int val) {
		coarseX = val & 0x1F;
	}
	
	public int getCoarseX() {
		return coarseX;
	}
}
