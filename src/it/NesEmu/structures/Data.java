package it.NesEmu.structures;

public class Data {

	private int data = 0x00;

	public int getData() {
		return data;
	}

	public void setData(int data) {
		this.data = data;
	}
}
