package it.NesEmu;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import it.NesEmu.structures.Instruction;

public class Cpu {

	private Bus bus;

	private static final int C = (1 << 0); // Carry flag
	private static final int Z = (1 << 1); // Zero flag
	private static final int I = (1 << 2); // Disable interrupts
	private static final int D = (1 << 3); // Decimal mode (non usato)
	private static final int B = (1 << 4); // Break operation called
	private static final int U = (1 << 5); // Unused
	private static final int V = (1 << 6); // Overflow
	private static final int N = (1 << 7); // Negative flag

	private int fetched = 0x00;
	private int addrAbs = 0x0000;
	private int addrRel = 0x0000;
	private int opcode = 0x00;
	private int cycles = 0;

	public int a = 0x00;
	public int x = 0x00;
	public int y = 0x00;
	public int stkp = 0x00;
	public int pc = 0x0000;
	public int status = 0x00;
	
	Instruction[] lookup = null;

	public Cpu() {
		lookup = new Instruction[] { new Instruction("BRK", "BRK", "IMM", 7),
				new Instruction("ORA", "ORA", "IZX", 6), new Instruction("???", "XXX", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 8), new Instruction("???", "NOP", "IMP", 3),
				new Instruction("ORA", "ORA", "ZP0", 3), new Instruction("ASL", "ASL", "ZP0", 5),
				new Instruction("???", "XXX", "IMP", 5), new Instruction("PHP", "PHP", "IMP", 3),
				new Instruction("ORA", "ORA", "IMM", 2), new Instruction("ASL", "ASL", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 2), new Instruction("???", "NOP", "IMP", 4),
				new Instruction("ORA", "ORA", "ABS", 4), new Instruction("ASL", "ASL", "ABS", 6),
				new Instruction("???", "XXX", "IMP", 6), new Instruction("BPL", "BPL", "REL", 2),
				new Instruction("ORA", "ORA", "IZY", 5), new Instruction("???", "XXX", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 8), new Instruction("???", "NOP", "IMP", 4),
				new Instruction("ORA", "ORA", "ZPX", 4), new Instruction("ASL", "ASL", "ZPX", 6),
				new Instruction("???", "XXX", "IMP", 6), new Instruction("CLC", "CLC", "IMP", 2),
				new Instruction("ORA", "ORA", "ABY", 4), new Instruction("???", "NOP", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 7), new Instruction("???", "NOP", "IMP", 4),
				new Instruction("ORA", "ORA", "ABX", 4), new Instruction("ASL", "ASL", "ABX", 7),
				new Instruction("???", "XXX", "IMP", 7), new Instruction("JSR", "JSR", "ABS", 6),
				new Instruction("AND", "AND", "IZX", 6), new Instruction("???", "XXX", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 8), new Instruction("BIT", "BIT", "ZP0", 3),
				new Instruction("AND", "AND", "ZP0", 3), new Instruction("ROL", "ROL", "ZP0", 5),
				new Instruction("???", "XXX", "IMP", 5), new Instruction("PLP", "PLP", "IMP", 4),
				new Instruction("AND", "AND", "IMM", 2), new Instruction("ROL", "ROL", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 2), new Instruction("BIT", "BIT", "ABS", 4),
				new Instruction("AND", "AND", "ABS", 4), new Instruction("ROL", "ROL", "ABS", 6),
				new Instruction("???", "XXX", "IMP", 6), new Instruction("BMI", "BMI", "REL", 2),
				new Instruction("AND", "AND", "IZY", 5), new Instruction("???", "XXX", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 8), new Instruction("???", "NOP", "IMP", 4),
				new Instruction("AND", "AND", "ZPX", 4), new Instruction("ROL", "ROL", "ZPX", 6),
				new Instruction("???", "XXX", "IMP", 6), new Instruction("SEC", "SEC", "IMP", 2),
				new Instruction("AND", "AND", "ABY", 4), new Instruction("???", "NOP", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 7), new Instruction("???", "NOP", "IMP", 4),
				new Instruction("AND", "AND", "ABX", 4), new Instruction("ROL", "ROL", "ABX", 7),
				new Instruction("???", "XXX", "IMP", 7), new Instruction("RTI", "RTI", "IMP", 6),
				new Instruction("EOR", "EOR", "IZX", 6), new Instruction("???", "XXX", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 8), new Instruction("???", "NOP", "IMP", 3),
				new Instruction("EOR", "EOR", "ZP0", 3), new Instruction("LSR", "LSR", "ZP0", 5),
				new Instruction("???", "XXX", "IMP", 5), new Instruction("PHA", "PHA", "IMP", 3),
				new Instruction("EOR", "EOR", "IMM", 2), new Instruction("LSR", "LSR", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 2), new Instruction("JMP", "JMP", "ABS", 3),
				new Instruction("EOR", "EOR", "ABS", 4), new Instruction("LSR", "LSR", "ABS", 6),
				new Instruction("???", "XXX", "IMP", 6), new Instruction("BVC", "BVC", "REL", 2),
				new Instruction("EOR", "EOR", "IZY", 5), new Instruction("???", "XXX", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 8), new Instruction("???", "NOP", "IMP", 4),
				new Instruction("EOR", "EOR", "ZPX", 4), new Instruction("LSR", "LSR", "ZPX", 6),
				new Instruction("???", "XXX", "IMP", 6), new Instruction("CLI", "CLI", "IMP", 2),
				new Instruction("EOR", "EOR", "ABY", 4), new Instruction("???", "NOP", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 7), new Instruction("???", "NOP", "IMP", 4),
				new Instruction("EOR", "EOR", "ABX", 4), new Instruction("LSR", "LSR", "ABX", 7),
				new Instruction("???", "XXX", "IMP", 7), new Instruction("RTS", "RTS", "IMP", 6),
				new Instruction("ADC", "ADC", "IZX", 6), new Instruction("???", "XXX", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 8), new Instruction("???", "NOP", "IMP", 3),
				new Instruction("ADC", "ADC", "ZP0", 3), new Instruction("ROR", "ROR", "ZP0", 5),
				new Instruction("???", "XXX", "IMP", 5), new Instruction("PLA", "PLA", "IMP", 4),
				new Instruction("ADC", "ADC", "IMM", 2), new Instruction("ROR", "ROR", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 2), new Instruction("JMP", "JMP", "IND", 5),
				new Instruction("ADC", "ADC", "ABS", 4), new Instruction("ROR", "ROR", "ABS", 6),
				new Instruction("???", "XXX", "IMP", 6), new Instruction("BVS", "BVS", "REL", 2),
				new Instruction("ADC", "ADC", "IZY", 5), new Instruction("???", "XXX", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 8), new Instruction("???", "NOP", "IMP", 4),
				new Instruction("ADC", "ADC", "ZPX", 4), new Instruction("ROR", "ROR", "ZPX", 6),
				new Instruction("???", "XXX", "IMP", 6), new Instruction("SEI", "SEI", "IMP", 2),
				new Instruction("ADC", "ADC", "ABY", 4), new Instruction("???", "NOP", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 7), new Instruction("???", "NOP", "IMP", 4),
				new Instruction("ADC", "ADC", "ABX", 4), new Instruction("ROR", "ROR", "ABX", 7),
				new Instruction("???", "XXX", "IMP", 7), new Instruction("???", "NOP", "IMP", 2),
				new Instruction("STA", "STA", "IZX", 6), new Instruction("???", "NOP", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 6), new Instruction("STY", "STY", "ZP0", 3),
				new Instruction("STA", "STA", "ZP0", 3), new Instruction("STX", "STX", "ZP0", 3),
				new Instruction("???", "XXX", "IMP", 3), new Instruction("DEY", "DEY", "IMP", 2),
				new Instruction("???", "NOP", "IMP", 2), new Instruction("TXA", "TXA", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 2), new Instruction("STY", "STY", "ABS", 4),
				new Instruction("STA", "STA", "ABS", 4), new Instruction("STX", "STX", "ABS", 4),
				new Instruction("???", "XXX", "IMP", 4), new Instruction("BCC", "BCC", "REL", 2),
				new Instruction("STA", "STA", "IZY", 6), new Instruction("???", "XXX", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 6), new Instruction("STY", "STY", "ZPX", 4),
				new Instruction("STA", "STA", "ZPX", 4), new Instruction("STX", "STX", "ZPY", 4),
				new Instruction("???", "XXX", "IMP", 4), new Instruction("TYA", "TYA", "IMP", 2),
				new Instruction("STA", "STA", "ABY", 5), new Instruction("TXS", "TXS", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 5), new Instruction("???", "NOP", "IMP", 5),
				new Instruction("STA", "STA", "ABX", 5), new Instruction("???", "XXX", "IMP", 5),
				new Instruction("???", "XXX", "IMP", 5), new Instruction("LDY", "LDY", "IMM", 2),
				new Instruction("LDA", "LDA", "IZX", 6), new Instruction("LDX", "LDX", "IMM", 2),
				new Instruction("???", "XXX", "IMP", 6), new Instruction("LDY", "LDY", "ZP0", 3),
				new Instruction("LDA", "LDA", "ZP0", 3), new Instruction("LDX", "LDX", "ZP0", 3),
				new Instruction("???", "XXX", "IMP", 3), new Instruction("TAY", "TAY", "IMP", 2),
				new Instruction("LDA", "LDA", "IMM", 2), new Instruction("TAX", "TAX", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 2), new Instruction("LDY", "LDY", "ABS", 4),
				new Instruction("LDA", "LDA", "ABS", 4), new Instruction("LDX", "LDX", "ABS", 4),
				new Instruction("???", "XXX", "IMP", 4), new Instruction("BCS", "BCS", "REL", 2),
				new Instruction("LDA", "LDA", "IZY", 5), new Instruction("???", "XXX", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 5), new Instruction("LDY", "LDY", "ZPX", 4),
				new Instruction("LDA", "LDA", "ZPX", 4), new Instruction("LDX", "LDX", "ZPY", 4),
				new Instruction("???", "XXX", "IMP", 4), new Instruction("CLV", "CLV", "IMP", 2),
				new Instruction("LDA", "LDA", "ABY", 4), new Instruction("TSX", "TSX", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 4), new Instruction("LDY", "LDY", "ABX", 4),
				new Instruction("LDA", "LDA", "ABX", 4), new Instruction("LDX", "LDX", "ABY", 4),
				new Instruction("???", "XXX", "IMP", 4), new Instruction("CPY", "CPY", "IMM", 2),
				new Instruction("CMP", "CMP", "IZX", 6), new Instruction("???", "NOP", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 8), new Instruction("CPY", "CPY", "ZP0", 3),
				new Instruction("CMP", "CMP", "ZP0", 3), new Instruction("DEC", "DEC", "ZP0", 5),
				new Instruction("???", "XXX", "IMP", 5), new Instruction("INY", "INY", "IMP", 2),
				new Instruction("CMP", "CMP", "IMM", 2), new Instruction("DEX", "DEX", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 2), new Instruction("CPY", "CPY", "ABS", 4),
				new Instruction("CMP", "CMP", "ABS", 4), new Instruction("DEC", "DEC", "ABS", 6),
				new Instruction("???", "XXX", "IMP", 6), new Instruction("BNE", "BNE", "REL", 2),
				new Instruction("CMP", "CMP", "IZY", 5), new Instruction("???", "XXX", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 8), new Instruction("???", "NOP", "IMP", 4),
				new Instruction("CMP", "CMP", "ZPX", 4), new Instruction("DEC", "DEC", "ZPX", 6),
				new Instruction("???", "XXX", "IMP", 6), new Instruction("CLD", "CLD", "IMP", 2),
				new Instruction("CMP", "CMP", "ABY", 4), new Instruction("NOP", "NOP", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 7), new Instruction("???", "NOP", "IMP", 4),
				new Instruction("CMP", "CMP", "ABX", 4), new Instruction("DEC", "DEC", "ABX", 7),
				new Instruction("???", "XXX", "IMP", 7), new Instruction("CPX", "CPX", "IMM", 2),
				new Instruction("SBC", "SBC", "IZX", 6), new Instruction("???", "NOP", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 8), new Instruction("CPX", "CPX", "ZP0", 3),
				new Instruction("SBC", "SBC", "ZP0", 3), new Instruction("INC", "INC", "ZP0", 5),
				new Instruction("???", "XXX", "IMP", 5), new Instruction("INX", "INX", "IMP", 2),
				new Instruction("SBC", "SBC", "IMM", 2), new Instruction("NOP", "NOP", "IMP", 2),
				new Instruction("???", "SBC", "IMP", 2), new Instruction("CPX", "CPX", "ABS", 4),
				new Instruction("SBC", "SBC", "ABS", 4), new Instruction("INC", "INC", "ABS", 6),
				new Instruction("???", "XXX", "IMP", 6), new Instruction("BEQ", "BEQ", "REL", 2),
				new Instruction("SBC", "SBC", "IZY", 5), new Instruction("???", "XXX", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 8), new Instruction("???", "NOP", "IMP", 4),
				new Instruction("SBC", "SBC", "ZPX", 4), new Instruction("INC", "INC", "ZPX", 6),
				new Instruction("???", "XXX", "IMP", 6), new Instruction("SED", "SED", "IMP", 2),
				new Instruction("SBC", "SBC", "ABY", 4), new Instruction("NOP", "NOP", "IMP", 2),
				new Instruction("???", "XXX", "IMP", 7), new Instruction("???", "NOP", "IMP", 4),
				new Instruction("SBC", "SBC", "ABX", 4), new Instruction("INC", "INC", "ABX", 7),
				new Instruction("???", "XXX", "IMP", 7) };
	}

	public void connectBus(Bus bus) {
		this.bus = bus;
	}

	public int read(int a) {
		return bus.cpuRead(a, false);
	}

	public void write(int a, int d) {
		bus.cpuWrite(a, d);
	}

	public void clock() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		if (cycles == 0) {
			opcode = read(pc);
			pc = (pc + 1) & 0xFFFF;
			
			cycles = lookup[opcode].cycles;
			byte additionalCycle1 = (byte) this.getClass().getMethod(lookup[opcode].addrmode).invoke(this);
			byte additionalCycle2 = (byte) this.getClass().getMethod(lookup[opcode].operate).invoke(this);

			cycles += (additionalCycle1 & additionalCycle2);
		}

		cycles--;
	}

	// Utility
	public boolean complete() {
		return cycles == 0;
	}

	// FUNZIONI DI BASE
	public void reset() {
		a = 0;
		x = 0;
		y = 0;
		stkp = 0xFD & 0xFF;
		status = 0x00 | U;

		addrAbs = 0xFFFC;
		int lo = read(addrAbs + 0);
		int hi = read((addrAbs + 1) & 0xFFFF);

		pc = ((hi << 8) | lo);

		addrAbs = 0x0000;
		addrRel = 0x0000;
		fetched = 0x00;

		cycles = 8;
	}

	public void irq() {
		// Push the program counter to the stack. It's 16-bits dont
		// forget so that takes two pushes
		write(0x0100 + stkp, (pc >>> 8) & 0x00FF);
		stkp = (stkp - 1) & 0xFF;
		write(0x0100 + stkp, pc & 0x00FF);
		stkp = (stkp - 1) & 0xFF;

		// Then Push the status register to the stack
		setFlag(B, false);
		setFlag(U, true);
		setFlag(I, true);
		write(0x0100 + stkp, status);
		stkp = (stkp - 1) & 0xFF;

		// Read new program counter location from fixed address
		addrAbs = 0xFFFE;
		int lo = read(addrAbs + 0);
		int hi = read((addrAbs + 1) & 0xFFFF);
		pc = ((hi << 8) | lo);

		// IRQs take time
		cycles = 7;
	}

	public void nmi() {
		// Push the program counter to the stack. It's 16-bits dont
		// forget so that takes two pushes
		write(0x0100 + stkp, (pc >> 8) & 0x00FF);
		stkp = (stkp - 1) & 0xFF;
		write(0x0100 + stkp, pc & 0x00FF);
		stkp = (stkp - 1) & 0xFF;

		// Then Push the status register to the stack
		setFlag(B, false);
		setFlag(U, true);
		setFlag(I, true);
		write((0x0100 + stkp), status);
		stkp = (stkp - 1) & 0xFF;

		// Read new program counter location from fixed address
		addrAbs = 0xFFFA;
		int lo = read(addrAbs + 0);
		int hi = read((addrAbs + 1) & 0xFFFF);
		pc = ((hi << 8) | lo);

		// IRQs take time
		cycles = 7;
	}

	// FUNZIONI SUI FLAG
	public int getFlag(int f) {
		return ((status & f) > 0) ? 1 : 0;
	}

	public void setFlag(int f, boolean v) {
		f &= 0xFF;

		if (v)
			status |= f;
		else
			status &= (~f & 0xFF);
	}

	// FETCH
	public int fetch() {
		if (!(lookup[opcode].addrmode.equals("IMP")))
			fetched = read(addrAbs);
		return fetched;
	}

	// ADDRESSING MODES

	public byte IMP() {
		fetched = a;
		return 0;
	}

	public byte IMM() {
		addrAbs = pc;
		pc = (pc + 1) & 0xFFFF;
		return 0;
	}

	public byte ZP0() {
		addrAbs = read(pc);
		pc = (pc + 1) & 0xFFFF;
		addrAbs &= 0x00FF;
		return 0;
	}

	public byte ZPX() {
		addrAbs = (read(pc) + x);
		pc = (pc + 1) & 0xFFFF;
		addrAbs &= 0x00FF;
		return 0;
	}

	public byte ZPY() {
		addrAbs = (read(pc) + y);
		pc = (pc + 1) & 0xFFFF;
		addrAbs &= 0x00FF;
		return 0;
	}

	public byte ABS() {
		int lo = read(pc);
		pc = (pc + 1) & 0xFFFF;
		int hi = read(pc);
		pc = (pc + 1) & 0xFFFF;

		addrAbs = ((hi << 8) | lo);

		return 0;
	}

	public byte ABX() {
		int lo = read(pc);
		pc = (pc + 1) & 0xFFFF;
		int hi = read(pc);
		pc = (pc + 1) & 0xFFFF;

		addrAbs = ((hi << 8) | lo);
		addrAbs += x;

		if ((addrAbs & 0xFF00) != (hi << 8))
			return 1;
		else
			return 0;
	}

	public byte ABY() {
		int lo = read(pc);
		pc = (pc + 1) & 0xFFFF;
		int hi = read(pc);
		pc = (pc + 1) & 0xFFFF;

		addrAbs = ((hi << 8) | lo);
		addrAbs = (addrAbs + y) & 0xFFFF;

		if ((addrAbs & 0xFF0) != (hi << 8))
			return 1;
		else
			return 0;
	}

	public byte IND() {
		int ptr_lo = read(pc);
		pc = (pc + 1) & 0xFFFF;
		int ptr_hi = read(pc);
		pc = (pc + 1) & 0xFFFF;

		int ptr = ((ptr_hi << 8) | ptr_lo);

		if (ptr_lo == 0x00FF) { // Simulate page boundary hardware bug
			addrAbs = ((read((ptr & 0xFF00)) << 8) | read((ptr + 0)));
		} else { // Behave normally
			addrAbs = ((read((ptr + 1)) << 8) | read((ptr + 0)));
		}

		return 0;
	}

	public byte IZX() {
		int t = read(pc);
		pc = (pc + 1) & 0xFFFF;

		int lo = read(((t + x) & 0x00FF));
		int hi = read(((t + x + 1) & 0x00FF));

		addrAbs = ((hi << 8) | lo);

		return 0;
	}

	public byte IZY() {
		int t = read(pc);
		pc = (pc + 1) & 0xFFFF;

		int lo = read((t & 0x00FF));
		int hi = read(((t + 1) & 0x00FF));

		addrAbs = ((hi << 8) | lo);
		addrAbs = (addrAbs + y) & 0xFFFF;

		if ((addrAbs & 0xFF00) != (hi << 8))
			return 1;
		else
			return 0;
	}

	public byte REL() {
		addrRel = read(pc);
		pc = (pc + 1) & 0xFFFF;
		if ((addrRel & 0x80) > 0)
			addrRel |= 0xFF00;
		return 0;
	}

	// Instruction IMPLEMENTATIONS
	public byte ADC() {

		fetch();

		int temp = (a + fetched + getFlag(C));

		setFlag(C, temp > 255);
		setFlag(Z, (temp & 0x00FF) == 0);
		setFlag(V, ((~(a ^ fetched) & (a ^ temp)) & 0x0080) > 0);
		setFlag(N, (temp & 0x80) == 128);

		a = temp & 0x00FF;

		return 1;
	}

	public byte AND() {
		fetch();
		a = a & fetched & 0xFF;
		setFlag(Z, a == 0x00);
		setFlag(N, (a & 0x80) == 128);
		return 1;
	}

	public byte ASL() {
		fetch();
		int temp = (fetched << 1);
		setFlag(C, (temp & 0xFF00) > 0);
		setFlag(Z, (temp & 0x00FF) == 0x00);
		setFlag(N, (temp & 0x80) == 128);
		if (lookup[opcode].addrmode.equals("IMP"))
			a = temp & 0x00FF;
		else
			write(addrAbs, temp & 0x00FF);
		return 0;
	}

	public byte BCC() {
		if (getFlag(C) == 0) {
			cycles++;
			addrAbs = (pc + addrRel) & 0xFFFF;

			if ((addrAbs & 0xFF00) != (pc & 0xFF00))
				cycles++;

			pc = addrAbs & 0xFFFF;
		}
		return 0;
	}

	public byte BCS() {
		if (getFlag(C) == 1) {
			cycles++;
			addrAbs = (pc + addrRel) & 0xFFFF;

			if ((addrAbs & 0xFF00) != (pc & 0xFF00))
				cycles++;

			pc = addrAbs;
		}
		return 0;
	}

	public byte BEQ() {
		if (getFlag(Z) == 1) {
			cycles++;
			addrAbs = (pc + addrRel) & 0xFFFF;

			if ((addrAbs & 0xFF00) != (pc & 0xFF00))
				cycles++;

			pc = addrAbs;
		}
		return 0;
	}

	public byte BIT() {
		fetch();
		int temp = a & fetched;
		setFlag(Z, (temp & 0x00FF) == 0x00);
		setFlag(N, (fetched & (1 << 7)) == 128);
		setFlag(V, (fetched & (1 << 6)) == 64);
		return 0;
	}

	public byte BMI() {
		if (getFlag(N) == 1) {
			cycles++;
			addrAbs = (pc + addrRel) & 0xFFFF;

			if ((addrAbs & 0xFF00) != (pc & 0xFF00))
				cycles++;

			pc = addrAbs;
		}
		return 0;
	}

	public byte BNE() {
		if (getFlag(Z) == 0) {
			cycles++;
			addrAbs = (pc + addrRel) & 0xFFFF;

			if ((addrAbs & 0xFF00) != (pc & 0xFF00))
				cycles++;

			pc = addrAbs;
		}
		return 0;
	}

	public byte BPL() {
		if (getFlag(N) == 0) {
			cycles++;
			addrAbs = (pc + addrRel) & 0xFFFF;

			if ((addrAbs & 0xFF00) != (pc & 0xFF00))
				cycles++;

			pc = addrAbs;
		}
		return 0;
	}

	public byte BRK() {
		pc = (pc + 1) & 0xFFFF;

		setFlag(I, true);
		write((0x0100 + stkp) & 0xFFFF, (pc >> 8) & 0x00FF);
		stkp = (stkp - 1) & 0xFF;
		write((0x0100 + stkp) & 0xFFFF, pc & 0x00FF);
		stkp = (stkp - 1) & 0xFF;

		setFlag(B, true);
		write((0x0100 + stkp) & 0xFFFF, status);
		stkp = (stkp - 1) & 0xFF;
		setFlag(B, false);

		pc = (read(0xFFFE) | (read(0xFFFF) << 8)) & 0xFFFF;
		return 0;

	}

	public byte BVC() {
		if (getFlag(V) == 0) {
			cycles++;
			addrAbs = (pc + addrRel) & 0xFFFF;

			if ((addrAbs & 0xFF00) != (pc & 0xFF00))
				cycles++;

			pc = addrAbs;
		}
		return 0;
	}

	public byte BVS() {
		if (getFlag(V) == 1) {
			cycles++;
			addrAbs = (pc + addrRel) & 0xFFFF;

			if ((addrAbs & 0xFF00) != (pc & 0xFF00))
				cycles++;

			pc = addrAbs;
		}
		return 0;
	}

	public byte CLC() {
		setFlag(C, false);
		return 0;
	}

	public byte CLD() {
		setFlag(D, false);
		return 0;
	}

	public byte CLI() {
		setFlag(I, false);
		return 0;
	}

	public byte CLV() {
		setFlag(V, false);
		return 0;
	}

	public byte CMP() {
		fetch();
		int temp = (a - fetched);
		setFlag(C, a >= fetched);
		setFlag(Z, (temp & 0x00FF) == 0x0000);
		setFlag(N, (temp & 0x0080) == 128);
		return 1;
	}

	public byte CPX() {
		fetch();
		int temp = (x - fetched) & 0xFF;
		setFlag(C, x >= fetched);
		setFlag(Z, (temp & 0x00FF) == 0x0000);
		setFlag(N, (temp & 0x0080) == 128);
		return 0;
	}

	public byte CPY() {
		fetch();
		int temp = (y - fetched) & 0xFF;
		setFlag(C, y >= fetched);
		setFlag(Z, (temp & 0x00FF) == 0x0000);
		setFlag(N, (temp & 0x0080) == 128);
		return 0;
	}

	public byte DEC() {
		fetch();
		int temp = (fetched - 1);
		write(addrAbs, temp & 0x00FF);
		setFlag(Z, (temp & 0x00FF) == 0x0000);
		setFlag(N, (temp & 0x0080) == 128);
		return 0;
	}

	public byte DEX() {
		x = (x - 1) & 0xFF;
		setFlag(Z, x == 0x00);
		setFlag(N, (x & 0x80) == 128);
		return 0;
	}

	public byte DEY() {
		y = (y - 1) & 0xFF;
		setFlag(Z, y == 0x00);
		setFlag(N, (y & 0x80) == 128);
		return 0;
	}

	public byte EOR() {
		fetch();
		a = (a ^ fetched) & 0xFF;
		setFlag(Z, a == 0x00);
		setFlag(N, (a & 0x80) == 128);
		return 1;
	}

	public byte INC() {
		fetch();
		int temp = (fetched + 1);
		write(addrAbs, temp & 0x00FF);
		setFlag(Z, (temp & 0x00FF) == 0x0000);
		setFlag(N, (temp & 0x0080) == 128);
		return 0;
	}

	public byte INX() {
		x = (x + 1) & 0xFF;
		setFlag(Z, x == 0x00);
		setFlag(N, (x & 0x80) == 128);
		return 0;
	}

	public byte INY() {
		y = (y + 1) & 0xFF;
		setFlag(Z, y == 0x00);
		setFlag(N, (y & 0x80) == 128);
		return 0;
	}

	public byte JMP() {
		pc = addrAbs;
		return 0;
	}

	public byte JSR() {
		pc = (pc - 1) & 0xFFFF;

		write(0x0100 + stkp, (pc >> 8) & 0x00FF);
		stkp = (stkp - 1) & 0xFF;
		write(0x0100 + stkp, pc & 0x00FF);
		stkp = (stkp - 1) & 0xFF;

		pc = addrAbs;
		return 0;
	}

	public byte LDA() {
		fetch();
		a = fetched & 0xFF;
		setFlag(Z, a == 0x00);
		setFlag(N, (a & 0x80) == 128);
		return 1;
	}

	public byte LDX() {
		fetch();
		x = fetched & 0xFF;
		setFlag(Z, x == 0x00);
		setFlag(N, (x & 0x80) == 128);
		return 1;
	}

	public byte LDY() {
		fetch();
		y = fetched & 0xFF;
		setFlag(Z, y == 0x00);
		setFlag(N, (y & 0x80) == 128);
		return 1;
	}

	public byte LSR() {
		fetch();
		setFlag(C, (fetched & 0x0001) == 1);
		int temp = (fetched >> 1);
		setFlag(Z, (temp & 0x00FF) == 0x0000);
		setFlag(N, (temp & 0x0080) == 128);
		if (lookup[opcode].addrmode.equals("IMP"))
			a = temp & 0x00FF;
		else
			write(addrAbs, temp & 0x00FF);
		return 0;
	}

	public byte NOP() {
		switch (opcode) {
		case 0x1C:
		case 0x3C:
		case 0x5C:
		case 0x7C:
		case 0xDC:
		case 0xFC:
			return 1;
		default:
		}
		return 0;
	}

	public byte ORA() {
		fetch();
		a = (a | fetched) & 0xFF;
		setFlag(Z, a == 0x00);
		setFlag(N, (a & 0x80) == 128);
		return 1;
	}

	public byte PHA() {
		write((0x0100 + stkp), a);
		stkp = (stkp - 1) & 0xFF;
		return 0;
	}

	public byte PHP() {
		write(0x0100 + stkp, status | B | U);
		setFlag(B, false);
		setFlag(U, false);
		stkp = (stkp - 1) & 0xFF;
		return 0;
	}

	public byte PLA() {
		stkp = (stkp + 1) & 0xFF;
		a = read(0x0100 + stkp) & 0xFF;
		setFlag(Z, a == 0x00);
		setFlag(N, (a & 0x80) == 128);
		return 0;
	}

	public byte PLP() {
		stkp = (stkp + 1) & 0xFF;
		status = read((0x0100 + stkp));
		setFlag(U, true);
		return 0;
	}

	public byte ROL() {
		fetch();
		int temp = ((fetched << 1) | getFlag(C));
		setFlag(C, (temp & 0xFF00) > 0);
		setFlag(Z, (temp & 0x00FF) == 0x0000);
		setFlag(N, (temp & 0x0080) == 128);
		if (lookup[opcode].addrmode.equals("IMP"))
			a = temp & 0xFF;
		else
			write(addrAbs, temp & 0x00FF);
		return 0;
	}

	public byte ROR() {
		fetch();
		int temp = ((getFlag(C) << 7) | (fetched >> 1));
		setFlag(C, (fetched & 0x01) == 1);
		setFlag(Z, (temp & 0x00FF) == 0x00);
		setFlag(N, (temp & 0x0080) == 128);
		if (lookup[opcode].addrmode.equals("IMP"))
			a = temp & 0x00FF;
		else
			write(addrAbs, temp & 0x00FF);
		return 0;
	}

	public byte RTI() {
		stkp = (stkp + 1) & 0xFF;
		status = read((0x0100 + stkp));
		status &= ~B;
		status &= ~U;

		stkp = (stkp + 1) & 0xFF;
		pc = read((0x0100 + stkp));
		stkp = (stkp + 1) & 0xFF;
		pc |= read((0x0100 + stkp)) << 8;
		return 0;
	}

	public byte RTS() {
		stkp = (stkp + 1) & 0xFF;
		pc = read((0x0100 + stkp));
		stkp = (stkp + 1) & 0xFF;
		pc = (pc | read((0x0100 + stkp)) << 8) & 0xFFFF;

		pc = (pc + 1) & 0xFFFF;
		return 0;
	}

	public byte SBC() {
		fetch();

		// Operating in 16-bit domain to capture carry out

		// We can invert the bottom 8 bits with bitwise xor
		int value = ((fetched) ^ 0x00FF);

		// Notice this is exactly the same as addition from here!
		int temp = (a + value + getFlag(C));
		setFlag(C, temp > 255);
		setFlag(Z, (temp & 0x00FF) == 0);
		setFlag(V, ((temp ^ a) & (temp ^ value) & 0x0080) > 0);
		setFlag(N, (temp & 0x80) == 128);

		a = (temp & 0x00FF);

		return 1;
	}

	public byte SEC() {
		setFlag(C, true);
		return 0;
	}

	public byte SED() {
		setFlag(D, true);
		return 0;
	}

	public byte SEI() {
		setFlag(I, true);
		return 0;
	}

	public byte STA() {
		write(addrAbs, a);
		return 0;
	}

	public byte STX() {
		write(addrAbs, x);
		return 0;
	}

	public byte STY() {
		write(addrAbs, y);
		return 0;
	}

	public byte TAX() {
		x = a & 0xFF;
		setFlag(Z, x == 0x00);
		setFlag(N, (x & 0x80) == 128);
		return 0;
	}

	public byte TAY() {
		y = a;
		setFlag(Z, y == 0x00);
		setFlag(N, (y & 0x80) == 128);
		return 0;
	}

	public byte TSX() {
		x = stkp & 0xFF;
		setFlag(Z, x == 0x00);
		setFlag(N, (x & 0x80) == 128);
		return 0;
	}

	public byte TXA() {
		a = x & 0xFF;
		setFlag(Z, a == 0x00);
		setFlag(N, (a & 0x80) == 128);
		return 0;
	}

	public byte TXS() {
		stkp = x & 0xFF;
		return 0;
	}

	public byte TYA() {
		a = y & 0xFF;
		setFlag(Z, a == 0x00);
		setFlag(N, (a & 0x80) == 128);
		return 0;
	}

	public byte XXX() {
		return 0;
	}
	
	public Map<Integer, String> disassemble(int start, int stop) {
		int addr = start;
		int value = 0x00;
		int lo = 0x00;
		int hi = 0x00;
		Map<Integer, String> mapLines = new HashMap<Integer, String>();
		int lineAddr = 0x0000;
		
		while(addr <= stop) {
			lineAddr = addr;
			
			String instr = "$" + hex(addr, 4) + ": ";
			int opcode = bus.cpuRead(addr,  true);
			addr++;
			instr += lookup[opcode].name + " ";
			
			if (lookup[opcode].addrmode.equals("IMP")) {
				instr += " {IMP}";
			}
			else if (lookup[opcode].addrmode.equals("IMM")) {
				value = bus.cpuRead(addr, true); addr++;
				instr += "#$" + hex(value, 2) + " {IMM}";
			}
			else if (lookup[opcode].addrmode.equals("ZP0"))
			{
				lo = bus.cpuRead(addr, true); addr++;
				hi = 0x00;												
				instr += "$" + hex(lo, 2) + " {ZP0}";
			}
			else if (lookup[opcode].addrmode.equals("ZPX"))
			{
				lo = bus.cpuRead(addr, true); addr++;
				hi = 0x00;														
				instr += "$" + hex(lo, 2) + ", X {ZPX}";
			}
			else if (lookup[opcode].addrmode.equals("ZPY"))
			{
				lo = bus.cpuRead(addr, true); addr++;
				hi = 0x00;														
				instr += "$" + hex(lo, 2) + ", Y {ZPY}";
			}
			else if (lookup[opcode].addrmode.equals("IZX"))
			{
				lo = bus.cpuRead(addr, true); addr++;
				hi = 0x00;								
				instr += "($" + hex(lo, 2) + ", X) {IZX}";
			}
			else if (lookup[opcode].addrmode.equals("IZY"))
			{
				lo = bus.cpuRead(addr, true); addr++;
				hi = 0x00;								
				instr += "($" + hex(lo, 2) + "), Y {IZY}";
			}
			else if (lookup[opcode].addrmode.equals("ABS"))
			{
				lo = bus.cpuRead(addr, true); addr++;
				hi = bus.cpuRead(addr, true); addr++;
				instr += "$" + hex((hi << 8) | lo, 4) + " {ABS}";
			}
			else if (lookup[opcode].addrmode.equals("ABX"))
			{
				lo = bus.cpuRead(addr, true); addr++;
				hi = bus.cpuRead(addr, true); addr++;
				instr += "$" + hex((hi << 8) | lo, 4) + ", X {ABX}";
			}
			else if (lookup[opcode].addrmode.equals("ABY"))
			{
				lo = bus.cpuRead(addr, true); addr++;
				hi = bus.cpuRead(addr, true); addr++;
				instr += "$" + hex((hi << 8) | lo, 4) + ", Y {ABY}";
			}
			else if (lookup[opcode].addrmode.equals("IND"))
			{
				lo = bus.cpuRead(addr, true); addr++;
				hi = bus.cpuRead(addr, true); addr++;
				instr += "($" + hex((hi << 8) | lo, 4) + ") {IND}";
			}
			else if (lookup[opcode].addrmode.equals("REL"))
			{
				value = bus.cpuRead(addr, true); addr++;
				instr += "$" + hex(value, 2) + " [$" + hex(addr + (byte)value, 4) + "] {REL}";
			}
			
			mapLines.put(lineAddr, instr);
		}
		
		return mapLines;
	}
	
	public String hex(int n, int d) {
		char[] s = new char[d];
		for(int i = d - 1; i >= 0; i--, n >>= 4) {
			s[i] = "0123456789ABCDEF".toCharArray()[n & 0xF];
		}
		
		return new String(s);
	}

}
