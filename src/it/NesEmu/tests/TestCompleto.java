package it.NesEmu.tests;

import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;

import it.NesEmu.Bus;
import it.NesEmu.Cartridge;

public class TestCompleto {

	private static Bus nes = new Bus();

	public static void main(String[] args)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, FileNotFoundException {

		nes.cpuWrite(0xFFFC, 0x00);
		nes.cpuWrite(0xFFFD, 0x80 & 0xFF);

		nes.insertCartridge(new Cartridge(args[0]));

		nes.reset();

		nes.cpu.pc = 0xC000;

		execute();
	}

	private static void execute() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		Scanner keyboard = new Scanner(System.in);

		for (int i = 0; i < 1000000; i++) {
			nes.clock();
		}

		printRegisters();
		printRam(0x0002, 16, 16);
		printRam(0x0003, 16, 16);

		keyboard.close();
	}

	public static void printRam(int startAddr, int rows, int columns) {
		for (int i = 0; i < rows; i++) {

			StringBuilder riga = new StringBuilder();
			riga.append("$" + convert(startAddr, true) + ": ");

			for (int j = 0; j < columns; j++) {
				riga.append(convert(nes.cpuRead(startAddr, true), false) + " ");
				startAddr++;
			}

			System.out.println(riga);
		}
		System.out.println("\n");
	}

	private static void printRegisters() {
		System.out.println("A: " + convert(nes.cpu.a, false) + " [" + nes.cpu.a + "]");
		System.out.println("X: " + convert(nes.cpu.x, false) + " [" + nes.cpu.x + "]");
		System.out.println("Y: " + convert(nes.cpu.y, false) + " [" + nes.cpu.y + "]");
		System.out.println("Stack p: " + convert(nes.cpu.stkp & 0xFF, false) + "\n");
	}

	public static String convert(int n, boolean isAddr) {
		String ret = Integer.toHexString(n);
		if (isAddr) {
			if (ret.length() == 3)
				ret = "0" + ret;
			if (ret.length() == 2)
				ret = "00" + ret;
			if (ret.length() == 1)
				ret = "000" + ret;
		} else {
			if (ret.length() == 1)
				ret = "0" + ret;
		}

		return ret.toUpperCase();
	}
}
