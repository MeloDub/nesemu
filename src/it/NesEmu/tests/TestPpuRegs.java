package it.NesEmu.tests;

import it.NesEmu.structures.Control;
import it.NesEmu.structures.LoopyRegister;
import it.NesEmu.structures.Mask;
import it.NesEmu.structures.Status;

public class TestPpuRegs {

	public static void main(String[] args) {
		
		Control control = new Control();
		LoopyRegister lr = new LoopyRegister();
		Mask mask = new Mask();
		Status status = new Status();
		
		control.setReg(0x56);
		System.out.println("Control.getReg() dovrebbe essere 0x56 (86): " + control.getReg());
		
		lr.setReg(0x95C6);
		System.out.println("LoopyRegister.getReg() dovrebbe essere 0x95C6 (38342): " + lr.getReg());
		
		mask.setReg(0x56);
		System.out.println("Mask.getReg() dovrebbe essere 0x56 (86): " + mask.getReg());
		
		status.setReg(0x56);
		System.out.println("Status.getReg() dovrebbe essere 0x56 (86): " + status.getReg());
	}

}
