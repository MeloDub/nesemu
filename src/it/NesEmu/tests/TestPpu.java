package it.NesEmu.tests;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import it.NesEmu.Bus;

public class TestPpu {

	public static Bus nes = new Bus();
	private static int scale = 3;

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			JFrame f = new JFrame("Test NES PPU");
			f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			f.add(nes.ppu.getScreen());
			f.addKeyListener(new KeyListener() {
				@Override
				public void keyTyped(KeyEvent e) {
					
				}
				
				@Override
				public void keyReleased(KeyEvent e) {
					TestPpu.nes.controller[0] &= e.getKeyCode() == KeyEvent.VK_A ? 			~0x80 : 0xFF;	  // A Button
					TestPpu.nes.controller[0] &= e.getKeyCode() == KeyEvent.VK_S ? 			~0x40 : 0xFF;     // B Button
					TestPpu.nes.controller[0] &= e.getKeyCode() == KeyEvent.VK_BACK_SPACE ? ~0x20 : 0xFF;     // Select
					TestPpu.nes.controller[0] &= e.getKeyCode() == KeyEvent.VK_ENTER ?		~0x10 : 0xFF;     // Start
					TestPpu.nes.controller[0] &= e.getKeyCode() == KeyEvent.VK_UP ? 		~0x08 : 0xFF;
					TestPpu.nes.controller[0] &= e.getKeyCode() == KeyEvent.VK_DOWN ? 		~0x04 : 0xFF;
					TestPpu.nes.controller[0] &= e.getKeyCode() == KeyEvent.VK_LEFT ? 		~0x02 : 0xFF;
					TestPpu.nes.controller[0] &= e.getKeyCode() == KeyEvent.VK_RIGHT ? 		~0x01 : 0xFF;
				}
				
				@Override
				public void keyPressed(KeyEvent e) {
					TestPpu.nes.controller[0] |= e.getKeyCode() == KeyEvent.VK_A ? 			0x80 : 0x00;     // A Button
					TestPpu.nes.controller[0] |= e.getKeyCode() == KeyEvent.VK_S ? 			0x40 : 0x00;     // B Button
					TestPpu.nes.controller[0] |= e.getKeyCode() == KeyEvent.VK_BACK_SPACE ? 0x20 : 0x00;     // Select
					TestPpu.nes.controller[0] |= e.getKeyCode() == KeyEvent.VK_ENTER ? 		0x10 : 0x00;     // Start
					TestPpu.nes.controller[0] |= e.getKeyCode() == KeyEvent.VK_UP ? 		0x08 : 0x00;
					TestPpu.nes.controller[0] |= e.getKeyCode() == KeyEvent.VK_DOWN ? 		0x04 : 0x00;
					TestPpu.nes.controller[0] |= e.getKeyCode() == KeyEvent.VK_LEFT ? 		0x02 : 0x00;
					TestPpu.nes.controller[0] |= e.getKeyCode() == KeyEvent.VK_RIGHT ? 		0x01 : 0x00;
				}
			});
			f.setSize(256 * scale + 700, 240 * scale + 39);
			f.setVisible(true);
			f.requestFocusInWindow();
		});
	}
}
