package it.NesEmu.tests;

import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;

import it.NesEmu.Bus;

public class TestCpu {

	private static Bus nes = new Bus();
	
	public static void main(String[] args) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
// 		Compilatore 6502: https://www.masswerk.at/6502/assembler.html
		
//		*=$8000
//		LDX #10
//		STX $0100
//		LDX #3
//		STX $0101
//		LDY $0100
//		LDA #0
//		CLC
//		loop
//		ADC $0101
//		DEY
//		BNE loop
//		STA $0102
//		NOP
//		NOP
//		NOP
//		STA $0102
//		NOP
//		NOP
//		NOP
		
		String[] program = "A2 0A 8E 00 01 A2 03 8E 01 01 AC 00 01 A9 00 18 6D 01 01 88 D0 FA 8D 02 01 EA EA EA 8D 02 01 EA EA EA".split(" ");
		int nOffset = 0;
		
		for(String s : program) {
			nes.cpuRam[nOffset] = Integer.parseInt(s, 16) & 0xFF;
			nOffset++;
		}
		
		nes.cpuWrite(0xFFFC, 0x00);
		nes.cpuWrite(0xFFFD, 0x80 & 0xFF);
		
		nes.cpu.reset();

		execute();
	}
	

	private static void execute() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		Scanner keyboard = new Scanner(System.in);
		
		while(!keyboard.nextLine().equals("q")) {
			do {
				nes.cpu.clock();
			}
			while (!nes.cpu.complete());
			
			printRegisters();
			printRam(0x0000, 16, 16);
			printRam(0x0100, 16, 16);
		}
		
		keyboard.close();
	}


	public static void printRam(int startAddr, int rows, int columns) {
		for(int i = 0; i < rows ; i++) {
			
			StringBuilder riga = new StringBuilder();
			riga.append("$" + convert(startAddr, true) + ": ");
			
			for(int j = 0; j < columns; j++) {
				riga.append(convert(nes.cpuRead(startAddr, true), false) + " ");
				startAddr++;
			}
			
			System.out.println(riga);
		}
		System.out.println("\n");
	}
	
	private static void printRegisters() {
		System.out.println("A: " + convert(nes.cpu.a, false) + " [" + nes.cpu.a + "]");
		System.out.println("X: " + convert(nes.cpu.x, false) + " [" + nes.cpu.x + "]");
		System.out.println("Y: " + convert(nes.cpu.y, false) + " [" + nes.cpu.y + "]");
		System.out.println("Stack p: " + convert(nes.cpu.stkp, false) + "\n");
	}

	public static String convert(int n, boolean isAddr) {
		  String ret = Integer.toHexString(n);
		  if(isAddr) {
			  if(ret.length() == 3)
				  ret = "0" + ret;
			  if(ret.length() == 2)
				  ret = "00" + ret;
			  if(ret.length() == 1)
				  ret = "000" + ret;
		  } else {
			  if(ret.length() == 1)
				  ret = "0" + ret;
		  }
		  
		  return ret.toUpperCase();
		}
}
