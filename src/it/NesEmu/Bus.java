package it.NesEmu;

import java.lang.reflect.InvocationTargetException;

import it.NesEmu.structures.Data;

public class Bus {
	
	public Cpu cpu = new Cpu();
	public Ppu ppu = new Ppu();
	public int[] cpuRam = new int[2048];
	public int[] controller = new int[2];
	public Cartridge cartridge;
	
	private int systemClockCounter = 0;
	private int[] controllerState = new int[2];
	
	private int dmaPage = 0x00; 
	private int dmaAddr = 0x00; 
	private int dmaData = 0x00; 
	private boolean dmaDummy = true;
	private boolean dmaTransfer = false;

	public Bus() {
		cpu.connectBus(this);
		
		// Ripulisco la ram per sicurezza
		for(int i = 0; i < 2048; i++ ) {
			cpuRam[i] = 0x00;	
		}
	}
	
	public void cpuWrite(int addr, int data) {
		addr &= 0xFFFF;
		
		if(cartridge != null && cartridge.cpuWrite(addr, data)) {
			
		}else if (addr >= 0x0000 && addr <= 0x1FFF) {
			cpuRam[addr & 0x07FF] = data & 0xFF;
		} 
		else if(addr >= 0x2000 && addr <= 0x3FFF) {
			ppu.cpuWrite(addr & 0x0007, data);
		}
		else if(addr == 0x4014) {
			dmaPage = data;
			dmaAddr = 0x00;
			dmaTransfer = true;
		}
		else if(addr >= 0x4016 && addr <= 0x4017) {
			controllerState[addr & 0x0001] = controller[addr & 0x0001];
		}
		
	}
	
	public int cpuRead(int addr, boolean bReadOnly) {
		addr &= 0xFFFF;
		Data data = new Data();
		
		if(cartridge != null && cartridge.cpuRead(addr, data)) {
			
		}
		else if (addr >= 0x0000 && addr <= 0x1FFF) {
			data.setData(cpuRam[addr & 0x07FF] & 0xFF);
		}
		else if(addr >= 0x2000 && addr <= 0x3FFF) {
			data.setData(ppu.cpuRead(addr & 0x0007, bReadOnly));
		}
		else if(addr >= 0x4016 && addr <= 0x4017) {
			data.setData((controllerState[addr & 0x0001] & 0x80) > 0? 1 : 0);
			controllerState[addr & 0x0001] = (controllerState[addr & 0x0001] << 1) & 0xFF;
		}

		return data.getData();
	}
	
	public void insertCartridge(final Cartridge cartridge) {
		this.cartridge = cartridge;
		ppu.connectCartridge(cartridge);
	}
	
	public void reset() {
		cartridge.reset();
		cpu.reset();
		ppu.reset();
		systemClockCounter = 0;
		dmaPage = 0x00;
		dmaAddr = 0x00;
		dmaData = 0x00;
		dmaDummy = true;
		dmaTransfer = false;
	}
	
	public void clock() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		ppu.clock();
		
		if(systemClockCounter % 3 == 0) {
			if(dmaTransfer) {
				if(dmaDummy) {
					if(systemClockCounter % 2 == 1)
						dmaDummy = false;
				} else {
					if(systemClockCounter % 2 == 0) {
						dmaData = cpuRead(dmaPage << 8 | dmaAddr, false);
					} else {
						ppu.setOamData(dmaAddr, dmaData);
						dmaAddr = (dmaAddr + 1) & 0xFF;
						if(dmaAddr == 0x00) {
							dmaTransfer = false;
							dmaDummy = true;
						}
					}
				}
			} else {
				cpu.clock();
			}
		}
		
		if(ppu.nmi) {
			ppu.nmi = false;
			cpu.nmi();
		}
		
		systemClockCounter++;
	}
}
